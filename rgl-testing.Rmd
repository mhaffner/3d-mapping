```{r setup, echo = TRUE, eval = TRUE, message = FALSE, warning = FALSE}
## required to create interactive 3d thing in Rmd
library(knitr)
knit_hooks$set(webgl = hook_webgl)
```

```{r echo = TRUE, eval = TRUE, message = FALSE, warning = FALSE, webgl = TRUE}
load("/home/matt/git-repos/3d-mapping/data/ec-dem-mat.RData")

## color definition of each point of the surface
height <- (max(ec.dem.mat, na.rm = TRUE) - min(ec.dem.mat, na.rm = TRUE))/max(ec.dem.mat, na.rm = TRUE)
r.prop <- height
g.prop <- 0
b.prop <- 1 - height
color  <- rgb(r.prop, g.prop, b.prop, maxColorValue=1)
col <- terrain.colors(10)[cut(ec.dem.mat, breaks = 10)]

x.scale <- extent(ec.dem)[2] - extent(ec.dem)[1]
y.scale <- extent(ec.dem)[4] - extent(ec.dem)[3]
## create 3d viewing object
rgl::persp3d(ec.dem.mat, col = col)

## change scaling; TODO: figure out how to automate this
aspect3d(x.scale, y.scale, 1000)
```
