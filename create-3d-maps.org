** Libraries and working directory
#+BEGIN_SRC R
  ## haffutils can be installed with devtools::install_gitlab("mhaffner/haffutils")
  library(haffutils)
  library(rgl)
  library(raster)
  library(sf)
  library(tmap)
  library(rgdal)
  library(rasterVis)

  setwd("/home/matt/git-repos/3d-mapping/data/")
#+END_SRC

** Get Wisconsin DEM and counties
#+BEGIN_SRC R
  ## download and unzip dem; wisconin dnr hosts this but not for direct download,
  ## so i put it in a git repo
  dl_file("https://gitlab.com/mhaffner/data/raw/master/DEM_30m.zip")

  ## cartographic boundary files from the US Census
  dl_file("https://www2.census.gov/geo/tiger/GENZ2017/shp/cb_2017_us_county_500k.zip")

  ## use readAll to load the raster in memory
  wi.dem <- readAll(raster("DEM_30m/demgw930/"))

  ## load county boundaries and transform crs into the one used by dem
  wi.counties <- readOGR("cb_2017_us_county_500k.shp") %>%
    subset(STATEFP == 55) %>%
    spTransform(., crs(wi.dem))

  ## plot them
  plot(wi.dem)
  plot(wi.counties, add = TRUE)
#+END_SRC

** Buffalo County
This makes for a potentially more interesting analysis since it has
greater elevation variation than Eau Claire.

#+BEGIN_SRC R
## get just buffalo county
buffalo.county <- readOGR("cb_2017_us_county_500k.shp") %>%
  subset(AFFGEOID == "0500000US55011") %>%
  spTransform(., crs(wi.dem))

## cut up the dem;
## crop gets everything in a rectangular area (and gets proper
## extents), then mask clips to the boundary that we want
buffalo.dem <- crop(wi.dem, buffalo.county) %>%
  mask(., buffalo.county)

## plot
plot(buffalo.dem)

## recalculate extent for the sake of plotting (or else it stays fixed to the
## entire state from the original dem)
## currently not working
#extent(buffalo.dem) <- extent(buffalo)

## plot it to verify (2d)
plot(buffalo.dem)

## 3d static; doesn't look great with defaults
rasterVis::plot3D(buffalo.dem)

## 3d interactive;
## convert to matrix first
buffalo.dem.mat <- as.matrix(buffalo.dem)
## create 3d viewing object
rgl::persp3d(buffalo.dem.mat)
## change the aspect ratio or else it doesn't look good
aspect3d(1, 1, 0.01)

#+END_SRC

** Eau Claire County
From this I think that mapping an entire county is just too much
without resampling.

#+BEGIN_SRC R
  ## get just eau claire county
  ec.co.boundary <- wi.counties[wi.counties$NAME == 'Eau Claire',]

  ## raster_clip combines crop and mask
  ec.dem <- raster_clip(wi.dem, ec.co.boundary)

  plot(ec.dem)

  ## 3d static; doesn't look great with defaults
  rasterVis::plot3D(ec.dem)

  ## convert to matrix first
  ec.dem.mat <- as.matrix(ec.dem)

  ## Color definition of each point of the surface
  height <- (max(ec.dem.mat, na.rm = TRUE) - min(ec.dem.mat, na.rm = TRUE))/max(ec.dem.mat, na.rm = TRUE)
  r.prop <- height
  g.prop <- 0
  b.prop <- 1 - height
  color  <- rgb(r.prop, g.prop, b.prop, maxColorValue=1)

  ## create 3d viewing object
  col <- terrain.colors(10)[cut(ec.dem.mat, breaks = 10)]
  #col <- topo.colors(10)[cut(ec.dem.mat, breaks = 10)]
  rgl::persp3d(ec.dem.mat, col = col)
  ## change the aspect ratio or else it doesn't look good
  x.scale <- extent(ec.dem)[2] - extent(ec.dem)[1]
  y.scale <- extent(ec.dem)[4] - extent(ec.dem)[3]
  aspect3d(y.scale, x.scale, 600)

  ## changes default view
  #view3d(theta = 20, phi = 260)
  ## probably need to resample to make this look better (if used at the county scale)
#+END_SRC

** Eau Claire (city)
#+BEGIN_SRC R
## get us urban areas from census
dl_file("www2.census.gov/geo/tiger/GENZ2017/shp/cb_2017_us_ua10_500k.zip")

places <- readOGR("cb_2017_us_ua10_500k.shp", verbose = FALSE)

## get just eau claire and transform crs into the crs of the dem
ec <- places[places$NAME10 == 'Eau Claire, WI',] %>%
  spTransform(., crs(wi.dem))

## clip raster to eau claire; here it makes sense to not use a mask as well
## since it would result in a weirdly shaped area, and a rectangle is more
## aesthetically pleasing
ec.dem <- crop(wi.dem, ec)

## plot
plot(ec.dem)

## convert raster to matrix
ec.dem.mat <- as.matrix(ec.dem)

## color definition of each point of the surface
height <- (max(ec.dem.mat, na.rm = TRUE) - min(ec.dem.mat, na.rm = TRUE))/max(ec.dem.mat, na.rm = TRUE)
r.prop <- height
g.prop <- 0
b.prop <- 1 - height
color  <- rgb(r.prop, g.prop, b.prop, maxColorValue=1)
col <- terrain.colors(10)[cut(ec.dem.mat, breaks = 10)]

x.scale <- extent(ec.dem)[2] - extent(ec.dem)[1]
y.scale <- extent(ec.dem)[4] - extent(ec.dem)[3]
## create 3d viewing object
x <- rgl::persp3d(ec.dem.mat, col = col)

## change scaling; TODO: figure out how to automate this
aspect3d(x.scale, y.scale, 1000)
#+END_SRC

